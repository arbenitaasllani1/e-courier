import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

     @override
      Widget build(BuildContext context) {

         return Scaffold(
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget> [
                        Image.asset('assets/images/logo.png', width: 100, height: 100),
                        SizedBox(height: 20),
                    ]
                )
            )
         );
      }
    }
  