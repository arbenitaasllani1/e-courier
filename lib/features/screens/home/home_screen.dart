import 'dart:convert';

import 'package:ecourier/features/models/product.dart';
import 'package:ecourier/features/screens/cart/cart_screen.dart';
import 'package:ecourier/features/screens/products/bags_screen.dart';
import 'package:ecourier/features/screens/products/clothes_screen.dart';
import 'package:ecourier/features/screens/home/notifications_screen.dart';
import 'package:ecourier/features/screens/home/special_offers.dart';
import 'package:ecourier/features/screens/orders/orders_screen.dart';
import 'package:ecourier/features/screens/products/electronics_screen.dart';
import 'package:ecourier/features/screens/products/favorite_products_screen.dart';
import 'package:ecourier/features/screens/products/jewellery_screen.dart';
import 'package:ecourier/features/screens/products/kitchen_screen.dart';
import 'package:ecourier/features/screens/products/shoes_screen.dart';
import 'package:ecourier/features/screens/products/toys_screen.dart';
import 'package:ecourier/features/screens/products/watch_screen.dart';
import 'package:ecourier/features/screens/profile/profile_screen.dart';
import 'package:ecourier/features/widgets/bottom_navigation/bottom_navigation_bar.dart';
import 'package:ecourier/features/widgets/button_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);


  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Widget> _children = [
    // const HomeScreen(),
    const CartScreen( cartProducts: []),
    const OrdersScreen(),
    const ProfileScreen(),
  ];


  int _selectedIndex = 0;
  List<Product> favoriteProducts = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    loadFavoriteProducts();
  }

  Future<void> loadFavoriteProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? favoriteProductStrings = prefs.getStringList('favoriteProducts');
    if (favoriteProductStrings != null) {
      setState(() {
        favoriteProducts = favoriteProductStrings.map((jsonString) => Product.fromJson(json.decode(jsonString))).toList();
        // isFavorite = favoriteProducts.any((product) => product.title == widget.title);
      });
    }
  }

  Future<void> saveFavoriteProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favoriteProductStrings = favoriteProducts.map((product) => product.toJsonString()).toList();
    await prefs.setStringList('favoriteProducts', favoriteProductStrings);
  }

  // Future<void> loadCartProducts() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   List<String>? cartProductStrings = prefs.getStringList('cartProducts');
  //   if(cartProductStrings != null) {
  //     setState(() {
  //       cartProducts = cartProductStrings.map((jsonString) => )
  //     });
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _children[_selectedIndex],
          Positioned(
            top: 20.0,
            left: 10.0,
            right: 10.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 80,
                  height: 80,
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Image.asset('images/profile.png'),
                  ),
                ),
                const SizedBox(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 5),
                      Text(
                        'Good Morning',
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 16.0,
                        ),
                      ),
                      // SizedBox(height: 10),
                      Text(
                        'Arbenita Asllani',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 30),

                SizedBox(
                  width: 50,
                  height: 50,
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const NotificationScreen()),
                      );
                    },
                    icon: const Icon(
                      Icons.notifications_outlined,
                      color: Colors.black87,
                      size: 30.0,
                    ),
                  ),
                ),
                SizedBox(
                  width: 50,
                  height: 50,
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FavoritesScreen(favoriteProducts: favoriteProducts),
                        ),
                      );
                    },
                    icon: const Icon(
                      Icons.favorite_outline,
                      color: Colors.black87,
                      size: 30.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 10.0,
            right: 10.0,
            top: MediaQuery
                .of(context)
                .size
                .height * 0.125,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              decoration: BoxDecoration(
                color: const Color(0xffe0e0e0),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: const TextField(
                decoration: InputDecoration(
                    hintText: 'Search',
                    hintStyle: TextStyle(
                      fontSize: 20.0,
                      color: Colors.black38,
                    ),
                    border: InputBorder.none,
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.search_rounded,
                        color: Colors.black38,
                        size: 30.0,
                      ),
                    ),
                    suffixIcon: Padding(padding: EdgeInsets.only(right: 10.0),
                      child: Icon(Icons.sort_sharp,
                        color: Colors.black54,
                        size: 28.0,
                      ),
                    )
                ),
              ),
            ),
          ),
          Positioned(
            left: 10.0,
            right: 10.0,
            height: MediaQuery
                .of(context)
                .size
                .height * 0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "Special offers",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const SpecialOffersScreen()),
                    );
                  },
                  child: const Text(
                    "See All",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 10.0,
            right: 10.0,
            top: MediaQuery
                .of(context)
                .size
                .height * 0.25,
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlay: true,
                autoPlayInterval: const Duration(seconds: 5),
                aspectRatio: 16 / 9,
                enlargeCenterPage: true,
                enlargeFactor: 0.7,

              ),
              items: const [
                Image(image: AssetImage('images/special_offer1.png')),
                Image(image: AssetImage('images/special_offer2.png')),
                Image(image: AssetImage('images/special_offer3.png')),
                Image(image: AssetImage('images/special_offer4.png')),
              ],
            ),
          ),
          Positioned(
            left: 20.0,
            right: 20.0,
            top: MediaQuery
                .of(context)
                .size
                .height * 0.52,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildRoundIconButton(Icons.computer_rounded, "Electronics", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const ElectronicsScreen()));
                }),
                _buildRoundIconButton(Icons.shopping_bag, "Bag", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const BagsScreen()));

                }),
                _buildRoundIconButton(Icons.electric_bike, "Shoes", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const ShoesScreen()));

                }),
                _buildRoundIconButton(Icons.shopping_bag_outlined, "Clothes", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const ClothesScreen()));
                }),
              ],
            ),
          ),
          Positioned(
            left: 20.0,
            right: 20.0,
            top: MediaQuery
                .of(context)
                .size
                .height * 0.65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildRoundIconButton(Icons.watch, "Watch", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const WatchesScreen()));

                }),
                _buildRoundIconButton(Icons.soup_kitchen, "Kitchen", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const KitchenScreen()));

                }),
                _buildRoundIconButton(Icons.smart_toy_sharp, "Toys", () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const ToysScreen()));

                }),
                _buildRoundIconButton(Icons.diamond_outlined, "Jewellery", () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const JewelleryScreen()));

                }),
              ],
            ),
          ),
          Positioned(
            left: 10.0,
              right: 10.0,
              top: MediaQuery
                  .of(context)
                  .size
                  .height * 0.78,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Most Popular",
                    style: TextStyle(
                    color: Colors.black,
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                   ),
                  ),
                  TextButton(onPressed: () {
                    print("see all button pressed");
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => const CartScreen(cartProducts: cartProducts)));
                  }, child: const Text("See All",
                    style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),))
                ],
              ),
          ),
          const Positioned(

            left: 20.0,
            right: 20.0,
            bottom: 40.0,
            child: Slidable(
              closeOnScroll: false,
              direction: Axis.horizontal,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ButtonWidget(text: "All"),
                    ButtonWidget(text: "Clothes"),
                    ButtonWidget(text: "Shoes"),
                    ButtonWidget(text: "Bags"),
                    ButtonWidget(text: "Electronics"),
                    ButtonWidget(text: "Watches"),
                    ButtonWidget(text: "Kitchen"),
                    ButtonWidget(text: "Toys"),
                    ButtonWidget(text: "Jewelry"),

                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigation(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
    );
  }

  // Widget _buildButton(String textData) {
  //   return Container(
  //     height: 50,
  //     decoration: const BoxDecoration(
  //       color:
  //     ),
  //   );
  // }

  Widget _buildRoundIconButton(IconData iconData, String textData, VoidCallback onPressed) {
    return Column(
      children: [
        Container(
          width: 70,
          height: 70,
          decoration: const BoxDecoration(
            color: Color(0xffe0e0e0),
            shape: BoxShape.circle,
          ),

          child: IconButton(
            onPressed: onPressed,
            icon: Icon(
              iconData,
              color: Colors.black87,
              size: 35.0,
            ),
          ),
        ),
        const SizedBox(height: 5),
        Text(
          textData,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}