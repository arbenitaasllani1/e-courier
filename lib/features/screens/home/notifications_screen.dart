import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Notification",
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.bold,

        ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _sectionTitle("Today"),
            _notificationsContainer(
              Icons.percent,
              "30% Special Discount!",
              "Special promotion only valid today",
            ),
            _sectionTitle("Yesterday"),
            _notificationsContainer(
              Icons.wallet,
              "Top Up E-Wallet Successful!",
              "You have to top up your e-wallet",
            ),
            _notificationsContainer(
              Icons.location_on,
              "New Services Available!",
              "Now you can track orders in real time",
            ),
            _sectionTitle("March 25, 2024"),
            _notificationsContainer(
              Icons.credit_card,
              "Credit Card Connected!",
              "Credit Card has been linked!",
            ),
            _notificationsContainer(
              Icons.person,
              "Account Setup Successful!",
              "Your account has been created!",
            ),
            _sectionTitle("March 12, 2024"),
            _notificationsContainer(
              Icons.percent,
              "30% Special Discount!",
              "Special promotion only valid today",
            ),
            _notificationsContainer(
              Icons.person,
              "Account Setup Successful!",
              "Your account has been created!",
            ),
          ],
        ),
      ),
    );
  }

  Widget _sectionTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: Text(
        title,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _notificationsContainer(
      IconData iconData,
      String textData1,
      String textData2,
      ) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        padding: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: const Color(0xffe0e0e0),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Row(
          children: [
            Container(
              width: 70,
              height: 70,
              decoration: const BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
              ),
              child: IconButton(
                onPressed: () {},
                icon: Icon(
                  iconData,
                  color: Colors.white,
                  size: 35.0,
                ),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    textData1,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    textData2,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
