import 'package:ecourier/features/models/product.dart';
import 'package:ecourier/features/screens/home/home_screen.dart';
import 'package:ecourier/features/screens/profile/profile_screen.dart';
import 'package:ecourier/features/widgets/bottom_navigation/bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatefulWidget {
  final List<Product> cartProducts;

  const CartScreen({
    Key? key,
    required this.cartProducts,
  }) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  // final List<Widget> _children = [
  //   // const HomeScreen(),
  //   // // const CartScreen(
  //   // //   cartProducts: [],
  //   // // ),
  //   // const ProfileScreen(),
  // ];

  int _selectedIndex = 1;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("My Cart", style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold,
      //   ),
      //   ),
      // ),
      body: ListView(
        children: [
          // _children[_selectedIndex],
        ],
      ),
      // bottomNavigationBar: BottomNavigation(
      // selectedIndex: _selectedIndex,
      // onItemTapped: _onItemTapped,
      // ),
    );
  }
}
