import 'package:ecourier/features/screens/intro/intro_page.dart';
import 'package:ecourier/features/screens/login_register/login_screen.dart';
import 'package:ecourier/features/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 10.0,
            left: 10.0,
            child: IconButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const IntroPage()),
                );
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 42.0,
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.15,
            left: 15.0,
            right: 15.0,
            child: Container(
              child: (
                const Text(
                  "Create your Account",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 48.0,
                    fontWeight: FontWeight.bold,

                  ),
                )
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.4,
            left: 15.0,
            right: 15.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),
                    prefixIconColor: Color(0xffa09f9e),
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Email",
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 30),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.lock),
                    prefixIconColor: const Color(0xffa09f9e),
                    filled: true,
                    fillColor: const Color(0xffe0e0e0),
                    hintText: "Password",
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.visibility_off),
                      onPressed: () {
                        print("icon pressed");
                      },
                      padding: const EdgeInsets.only(right: 20.0),

                    ),
                    suffixIconColor: const Color(0xffa09f9e),
                    border: const OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const CheckboxWidget(),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  width: double.infinity,
                  height: 50.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const ProfileScreen()),
                      );
                    },
                    child: const Text(
                      'Sign up',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 50.0),
                const Text(
                 "or continue with",
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Positioned(
                  left: 20.0,
                  right: 20.0,
                  // bottom: MediaQuery.of(context).size.height * 0.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: IconButton(
                          icon: Image.asset('images/facebook.png'),
                          onPressed: () {
                            print("Button pressed");
                          },
                        ),
                      ),
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: IconButton(
                          icon: Image.asset('images/google.png'),
                          onPressed: () {
                            print("Button pressed");
                          },
                        ),
                      ),
                      SizedBox(
                        width: 100,
                        height: 100,
                        child: IconButton(
                          icon: Image.asset('images/apple2.png'),
                          onPressed: () {
                            print("Button pressed");
                          },
                        ),
                      ),
                    ],
                  ),
                ),

                const SizedBox(height: 10.0),
                Row(

                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "Already have an account?",
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const LoginScreen()),
                        );
                      },
                      child: const Text(
                        "Sign in",
                        style: TextStyle(
                          color: Colors.black45,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CheckboxWidget extends StatefulWidget {
  const CheckboxWidget({super.key});

  @override
  _CheckboxWidgetState createState() => _CheckboxWidgetState();
}

class _CheckboxWidgetState extends State<CheckboxWidget> {
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Checkbox(
            value: _isChecked,
            activeColor: Colors.black,
            onChanged: (bool? value) {
              setState(() {
                _isChecked = value!;
              });
            },
          ),
          const Text(
            "Remember me",
            style: TextStyle(
              color: Color(0xff616262),
              fontSize: 14.0,
              fontWeight: FontWeight.bold
            ),
          )
        ],
      ),
    );
  }
}