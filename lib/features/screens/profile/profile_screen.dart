import 'package:ecourier/features/screens/home/home_screen.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  int _selectedIndex = 3;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final TextEditingController _dateController = TextEditingController();
  // String _selectedCountry = 'Kosova (+383)';
  //
  // final List<String> countries = [
  //   'Afghanistan	(+93)',
  //   'Albania (+355)',
  //   'Belgium (+32)',
  //   'Canada (+1)',
  //   'Denmark (+45)',
  //   'Kosova (+383)',
  //   'United States (+1)'
  // ];

  final List<String> gender = [
    'Female',
    'Male',
    'Rather not say'
  ];

  String? _selectedGender;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 30.0,
            left: 10.0,
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 40.0,
                  ),
                  padding: const EdgeInsets.only(right: 20.0),
                ),
                const Text(
                  "Fill Your Profile",
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: 100.0,
            left: 120.0,
            right: 120.0,
            child: AspectRatio(
              aspectRatio: 1 / 1,
              child: Center(
                child: Image.asset(
                  'images/profile.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.35,
            left: 15.0,
            right: 15.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const TextField(
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Full Name",
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                const TextField(
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Nickname",
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                TextField(
                  controller: _dateController,
                  readOnly: true,
                  onTap: () async {
                    DateTime? selectedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1900),
                      lastDate: DateTime.now(),
                    );
                    if (selectedDate != null) {
                      setState(() {
                        _dateController.text =
                        selectedDate.toString().split(' ')[0];
                      });
                    }
                  },
                  decoration: const InputDecoration(
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Date of Birth",
                    suffixIcon: Icon(Icons.calendar_month_outlined),
                    suffixIconColor: Color(0xffa09f9e),
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                const TextField(
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Email",
                    suffixIcon: Icon(Icons.email_outlined),
                    suffixIconColor: Color(0xffa09f9e),
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                const TextField(
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffe0e0e0),
                    hintText: "Phone Number",
                    suffixIcon: Icon(Icons.phone),
                    suffixIconColor: Color(0xffa09f9e),
                    border: OutlineInputBorder(
                      borderSide:  BorderSide.none,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.3,
                          child: ListView.builder(
                            itemCount: gender.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(gender[index]),
                                onTap: () {
                                  setState(() {
                                    _selectedGender = gender[index];
                                  });
                                  Navigator.pop(context);
                                },
                              );
                            },
                          ),
                        );
                      },
                    );
                  },
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(

                      color: const Color(0xffe0e0e0),
                      borderRadius: BorderRadius.circular(10.0),
                     // border: BorderSide.none,
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _selectedGender ?? "Select Gender",
                          style: const TextStyle(
                            color: Colors.black54,
                            fontSize: 16.0,
                          ),
                        ),
                        const Icon(
                            Icons.arrow_drop_down,
                          color: Color(0xff000000),
                          size: 30.0,
                        ),
                      ],
                    ),
                  )

                ),
                const SizedBox(height: 40),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10.0),
                  width: double.infinity,
                  height: 60.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                    ),
                    onPressed: () {
                      // print("Continue button pressed");
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const HomeScreen()),
                      );
                    },
                    child: const Text(
                      'Continue',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
