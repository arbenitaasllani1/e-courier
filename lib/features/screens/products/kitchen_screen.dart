import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:flutter/material.dart';

class KitchenScreen extends StatelessWidget {
  const KitchenScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Kitchen",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                                  imageUrl: 'kitchen/kitchen1.jpg',
                                  title: "Plate Set",
                                  rating: 4.6,
                                  price: 120.00,
                                  itemSold: '437 sold',
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen1.jpg',
                    title: 'Plate Set',
                    rating: 4.6,
                    price: 120.00,
                    itemSold: "437 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'kitchen/kitchen2.jpg',
                              title: "Knife Set",
                              rating: 4.6,
                              price: 230.00,
                              itemSold: '4,378 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen2.jpg',
                    title: 'Knife Set',
                    rating: 4.6,
                    price: 230.00,
                    itemSold: "4,378 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'kitchen/kitchen3.jpg',
                              title: "Cooking Pots and Pans Set",
                              rating: 4.4,
                              price: 760.00,
                              itemSold: '2,485 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen3.jpg',
                    title: 'Cooking Pots and Pans Set',
                    rating: 4.4,
                    price: 760.00,
                    itemSold: "2,485 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'kitchen/kitchen4.jpg',
                              title: "Cuisinart Knife Set",
                              rating: 4.9,
                              price: 320.00,
                              itemSold: '1,437 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen4.jpg',
                    title: 'Cuisinart Knife Set',
                    rating: 4.9,
                    price: 320.00,
                    itemSold: "1,437 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'kitchen/kitchen5.jpg',
                              title: "Stand Mixer",
                              rating: 4.9,
                              price: 990.00,
                              itemSold: '3,437 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen5.jpg',
                    title: 'Stand Mixer',
                    rating: 4.9,
                    price: 990.00,
                    itemSold: "3,437 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'kitchen/kitchen6.jpg',
                              title: "Slicia Siver Utensil Set",
                              rating: 4.8,
                              price: 260.00,
                              itemSold: '9,526 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'kitchen/kitchen6.jpg',
                    title: 'Slicia Siver Utensil Set',
                    rating: 4.8,
                    price: 260.00,
                    itemSold: "9,526 sold",
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
