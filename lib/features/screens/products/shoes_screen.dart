import 'package:ecourier/features/widgets/details/shoes_details.dart';
import 'package:flutter/material.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';

class ShoesScreen extends StatelessWidget {
  const ShoesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Shoes",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes1.jpg',
                          title: 'Flared Heel Leather Mules',
                          rating: 4.8,
                          price: 320.00,
                          itemSold: "9,742 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '37',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes1.jpg',
                    title: 'Flared Heel Leather Mules',
                    rating: 4.8,
                    price: 320.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes2.jpg',
                          title: 'Dolce & Gabbana Butterfly Heels',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '36',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes2.jpg',
                    title: 'Dolce & Gabbana Butterfly Heels',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes3.jpg',
                          title: 'Adidas Spezial',
                          rating: 4.7,
                          price: 385.00,
                          itemSold: "7,482 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '40',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes3.jpg',
                    title: 'Adidas Spezial',
                    rating: 4.7,
                    price: 385.00,
                    itemSold: "7,482 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes4.jpg',
                          title: 'Adidas Blue Samba',
                          rating: 4.8,
                          price: 410.00,
                          itemSold: "8,174 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '40',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes4.jpg',
                    title: 'Adidas Blue Samba',
                    rating: 4.8,
                    price: 410.00,
                    itemSold: "8,174 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes5.jpg',
                          title: 'Reebok Sneakers',
                          rating: 4.4,
                          price: 320.00,
                          itemSold: "9,742 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '39',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes5.jpg',
                    title: 'Reebok Sneakers',
                    rating: 4.4,
                    price: 320.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ShoesDetailScreen(
                          imageUrl: 'shoes/shoes6.jpg',
                          title: 'Birkenstock Arizona Sandals',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: '38',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'shoes/shoes6.jpg',
                    title: 'Birkenstock Arizona Sandals',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
