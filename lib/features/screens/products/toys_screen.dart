import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:flutter/material.dart';

class ToysScreen extends StatelessWidget {
  const ToysScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Toys",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy1.jpg',
                              title: 'Wooderful Life',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy1.jpg',
                    title: 'Wooderful Life',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy2.jpg',
                              title: 'Wooden Fruit Stacker',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy2.jpg',
                    title: 'Wooden Fruit Stacker',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy3.jpg',
                              title: 'Handmade Wooden Train',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy3.jpg',
                    title: 'Handmade Wooden Train',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy4.jpg',
                              title: 'Wooden Giraffe',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy4.jpg',
                    title: 'Wooden Giraffe',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy5.jpg',
                              title: 'Wooden Tray Puzzle',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy5.jpg',
                    title: 'Wooden Tray Puzzle',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            const BagsElectronicsDetailScreen(
                              imageUrl: 'toys/toy6.jpg',
                              title: 'Wooderful Life Toy',
                              rating: 4.5,
                              price: 320.00,
                              itemSold: "3,432 sold",
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'toys/toy6.jpg',
                    title: 'Wooderful Life Toy',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
