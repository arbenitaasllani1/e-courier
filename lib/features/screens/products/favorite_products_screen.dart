import 'package:ecourier/features/models/product.dart';
// import 'package:ecourier/features/widgets/details/product_details_screen.dart';
import 'package:flutter/material.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Product> favoriteProducts;

  const FavoritesScreen({Key? key, required this.favoriteProducts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Wishlist'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: (favoriteProducts.length / 2).ceil(),
              itemBuilder: (context, index) {
                final int startIndex = index * 2;
                final int endIndex = startIndex + 1;

                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            _navigateToDetails(
                                context, favoriteProducts[startIndex]);
                          },
                          child: _buildProductItem(
                              context, favoriteProducts[startIndex]),
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      if (endIndex < favoriteProducts.length)
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              _navigateToDetails(
                                  context, favoriteProducts[endIndex]);
                            },
                            child: _buildProductItem(
                                context, favoriteProducts[endIndex]),
                          ),
                        ),
                    ],
                  ),
                );
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.all(20.0),
            height: 50.0,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
              ),
              onPressed: () {
                print("go to checkout clicked");
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.shopping_bag, color: Colors.white),
                  SizedBox(width: 10),
                  Text(
                    "Go To Checkout",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProductItem(BuildContext context, Product product) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.transparent,
        ),
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              flex: 8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AspectRatio(
                    aspectRatio: 3 / 4,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset(
                        product.imageUrl,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    product.title,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text('\$${product.price.toStringAsFixed(2)}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _navigateToDetails(BuildContext context, Product product) {
    // Navigator.push(
    //   context, MaterialPageRoute(builder: (context) => ProductDetailsScreen(product: product)),
    // );
    print('Navigating to details of ${product.title}');
  }
}
