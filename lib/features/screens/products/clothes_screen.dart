import 'package:ecourier/features/widgets/selector/size_selector.dart';
import 'package:flutter/material.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:ecourier/features/widgets/details/clothes_details.dart';

class ClothesScreen extends StatefulWidget {
  const ClothesScreen({Key? key}) : super(key: key);

  @override
  State<ClothesScreen> createState() => _ClothesScreenState();
}

class _ClothesScreenState extends State<ClothesScreen> {
  String selectedSize = '';

  void _updateSelectedSize(String size) {
    setState(() {
      selectedSize = size;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Clothes",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    // setState(() {
                    //   selectedSize = '';
                    // });
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes1.jpg',
                          title: 'Venesa Long Shirt',
                          rating: 4.8,
                          price: 320.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'M',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes1.jpg',
                    title: 'Venesa Long Shirt',
                    rating: 4.8,
                    price: 320.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes2.jpg',
                          title: 'Werolla Cardigans',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'S',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes2.jpg',
                    title: 'Werolla Cardigans',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes3.jpg',
                          title: 'Viyara Ma Blazer',
                          rating: 4.7,
                          price: 385.00,
                          itemSold: "7,482 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'L',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes3.jpg',
                    title: 'Viyara Ma Blazer',
                    rating: 4.7,
                    price: 385.00,
                    itemSold: "7,482 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes4.jpg',
                          title: 'Moco Blue Suit',
                          rating: 4.8,
                          price: 410.00,
                          itemSold: "8,174 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'XL',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes4.jpg',
                    title: 'Moco Blue Suit',
                    rating: 4.8,
                    price: 410.00,
                    itemSold: "8,174 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes5.jpg',
                          title: 'Venesa Long Blouse',
                          rating: 4.4,
                          price: 320.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'M',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes5.jpg',
                    title: 'Venesa Long Blouse',
                    rating: 4.4,
                    price: 320.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ClothesDetailScreen(
                          imageUrl: 'clothes/clothes6.jpg',
                          title: 'Werolla Shirt',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                          selectedSize: 'S',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'clothes/clothes6.jpg',
                    title: 'Werolla Shirt',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),

            ],
          ),
          SizeSelector(
            sizes: const ['S', 'M', 'L', 'XL'],
            selectedSize: selectedSize,
            onSizeSelected: _updateSelectedSize,
          ),
        ],
      ),
    );
  }
}
