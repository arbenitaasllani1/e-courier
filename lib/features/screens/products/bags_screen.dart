import 'package:flutter/material.dart';
import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';

class BagsScreen extends StatelessWidget {
  const BagsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Bags",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag1.jpg',
                          title: 'Annelisse Shoulder Bag',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag1.jpg',
                    title: 'Annelisse Shoulder Bag',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag2.jpg',
                          title: 'Navy Carey Crescent hobo Bag',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag2.jpg',
                    title: 'Navy Carey Crescent hobo Bag',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag3.jpg',
                          title: 'Navy Saddle Bag',
                          rating: 4.7,
                          price: 385.00,
                          itemSold: "7,482 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag3.jpg',
                    title: 'Navy Saddle Bag',
                    rating: 4.7,
                    price: 385.00,
                    itemSold: "7,482 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag4.jpg',
                          title: 'YSL Leather Shoulder Bag',
                          rating: 4.8,
                          price: 410.00,
                          itemSold: "8,174 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag4.jpg',
                    title: 'YSL Leather Shoulder Bag',
                    rating: 4.8,
                    price: 410.00,
                    itemSold: "8,174 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag5.jpg',
                          title: 'Jacquemus Bag',
                          rating: 4.4,
                          price: 320.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag5.jpg',
                    title: 'Jacquemus Bag',
                    rating: 4.4,
                    price: 320.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BagsElectronicsDetailScreen(
                          imageUrl: 'bags/bag6.jpg',
                          title: 'Vintage Shoulder Bag',
                          rating: 4.9,
                          price: 345.00,
                          itemSold: "9,742 sold",
                          description:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                        ),
                      ),
                    );
                  },
                  child: const ProductWidget(
                    imageUrl: 'bags/bag6.jpg',
                    title: 'Vintage Shoulder Bag',
                    rating: 4.9,
                    price: 345.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
