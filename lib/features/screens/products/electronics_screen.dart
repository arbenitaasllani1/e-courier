import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:flutter/material.dart';

class ElectronicsScreen extends StatelessWidget {
  const ElectronicsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Electronics",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec1.jpg',
                                  title: 'Instax Photo Printer',
                                  rating: 4.5,
                                  price: 100.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec1.jpg',
                    title: 'Instax Photo Printer',
                    rating: 4.5,
                    price: 100.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec2.jpg',
                                  title: 'MacBook Pro M2 16-inch',
                                  rating: 4.5,
                                  price: 1600.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec2.jpg',
                    title: 'MacBook Pro M2 16-inch',
                    rating: 4.5,
                    price: 1600.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec3.jpg',
                                  title: 'Instax Camera',
                                  rating: 4.5,
                                  price: 450.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec3.jpg',
                    title: 'Instax Camera',
                    rating: 4.5,
                    price: 450.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec4.jpg',
                                  title: 'Canon Professional Camera',
                                  rating: 4.5,
                                  price: 320.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec4.jpg',
                    title: 'Canon Professional Camera',
                    rating: 4.5,
                    price: 320.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec5.jpg',
                                  title: 'AirPods Max',
                                  rating: 4.5,
                                  price: 530.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec5.jpg',
                    title: 'AirPods Max',
                    rating: 4.5,
                    price: 530.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'electronics/elec6.jpg',
                                  title: 'Apple Watch Series 9',
                                  rating: 4.5,
                                  price: 640.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'electronics/elec6.jpg',
                    title: 'Apple Watch Series 9',
                    rating: 4.5,
                    price: 640.00,
                    itemSold: "3,432 sold",
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
