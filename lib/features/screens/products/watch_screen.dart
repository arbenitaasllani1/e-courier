import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:flutter/material.dart';

class WatchesScreen extends StatelessWidget {
  const WatchesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Watches",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                                  imageUrl: 'watches/watches1.jpg',
                                  title: "Cartier Watch",
                                  rating: 4.9,
                                  price: 8000.00,
                                  itemSold: '14,732 sold',
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches1.jpg',
                    title: 'Cartier Watch',
                    rating: 4.9,
                    price: 8000.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'watches/watches2.jpg',
                              title: "Chanel Watch",
                              rating: 4.9,
                              price: 5400.00,
                              itemSold: '14,732 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches2.jpg',
                    title: 'Chanel Watch',
                    rating: 4.9,
                    price: 5400.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'watches/watches3.jpg',
                              title: "Anne Klein Diamond Watch",
                              rating: 4.9,
                              price: 10500.00,
                              itemSold: '14,732 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches3.jpg',
                    title: 'Anne Klein Diamond Watch',
                    rating: 4.9,
                    price: 10500.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'watches/watches4.jpg',
                              title: "Daniel Wellington Watch",
                              rating: 4.9,
                              price: 2600.00,
                              itemSold: '14,732 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches4.jpg',
                    title: 'Daniel Wellington Watch',
                    rating: 4.9,
                    price: 2600.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'watches/watches5.jpg',
                              title: "Patek Philippe Watch",
                              rating: 4.9,
                              price: 26000.00,
                              itemSold: '14,732 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches5.jpg',
                    title: 'Patek Philippe Watch',
                    rating: 4.9,
                    price: 26000.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                              imageUrl: 'watches/watches6.jpg',
                              title: "Longnes Watch",
                              rating: 4.9,
                              price: 600.00,
                              itemSold: '14,732 sold',
                              description:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                            )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'watches/watches6.jpg',
                    title: 'Longnes Watch',
                    rating: 4.9,
                    price: 600.00,
                    itemSold: "14,732 sold",
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
