import 'package:ecourier/features/widgets/details/bags_details.dart';
import 'package:ecourier/features/widgets/products/product_widget.dart';
import 'package:flutter/material.dart';

class JewelleryScreen extends StatelessWidget {
  const JewelleryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Jewellery",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'jewellery/jewel1.jpg',
                                  title: 'Van Cleef & Arpels Bracelet',
                                  rating: 4.5,
                                  price: 4200.00,
                                  itemSold: "3,432 sold",
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel1.jpg',
                    title: 'Van Cleef & Arpels Bracelet',
                    rating: 4.8,
                    price: 4200.00,
                    itemSold: "9,742 sold",
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                                imageUrl: 'jewellery/jewel2.jpg',
                                title: 'Gold Hoop Earrings',
                                rating: 4.9,
                                price: 900.00,
                                itemSold: '3,546 sold',
                                description:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')));

                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel2.jpg',
                    title: 'Gold Hoop Earrings',
                    rating: 4.9,
                    price: 900.00,
                    itemSold: '3,546 sold',
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                                imageUrl: 'jewellery/jewel3.jpg',
                                title: 'Gold Layered Necklace',
                                rating: 4.9,
                                price: 870.00,
                                itemSold: '3,546 sold',
                                description:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')));

                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel3.jpg',
                    title: 'Gold Layered Necklace',
                    rating: 4.9,
                    price: 870.00,
                    itemSold: '3,546 sold',
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BagsElectronicsDetailScreen(
                                imageUrl: 'jewellery/jewel4.jpg',
                                title: 'Diamond Gold Ring',
                                rating: 4.9,
                                price: 1250.00,
                                itemSold: '3,546 sold',
                                description:
                                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')));
                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel4.jpg',
                    title: 'Diamond Gold Ring',
                    rating: 4.9,
                    price: 1250.00,
                    itemSold: '3,546 sold',
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'jewellery/jewel5.jpg',
                                  title: 'Gold Vintage Ring',
                                  rating: 4.9,
                                  price: 1670.00,
                                  itemSold: '3,546 sold',
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel5.jpg',
                    title: 'Gold Vintage Ring',
                    rating: 4.9,
                    price: 1670.00,
                    itemSold: '3,546 sold',
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const BagsElectronicsDetailScreen(
                                  imageUrl: 'jewellery/jewel6.jpg',
                                  title: 'Cartier Gold LOVE Bracelet ',
                                  rating: 4.9,
                                  price: 6400.00,
                                  itemSold: '3,546 sold',
                                  description:
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
                                )));
                  },
                  child: const ProductWidget(
                    imageUrl: 'jewellery/jewel6.jpg',
                    title: 'Cartier Gold LOVE Bracelet',
                    rating: 4.9,
                    price: 6400.00,
                    itemSold: '3,546 sold',
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
