// import 'package:flutter/material.dart';
//
// class NotificationWidget extends StatefulWidget {
//   const NotificationWidget({Key? key, required this.text}) : super(key: key);
//
//   final String text;
//
//   @override
//   _NotificationWidgetState createState() => _NotificationWidgetState();
// }
//
// class _NotificationWidgetState extends State<NotificationWidget> {
//
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//
//         child: Padding(
//           padding: const EdgeInsets.only(right: 10.0),
//           child: Container(
//             height: 80,
//             width: double.infinity,
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(20),
//               border: Border.all(color: Colors.white, width: 2.0),
//             ),
//
//               child: Row(
//                 children: [
//                   Container(
//                     width: 70,
//                     height: 70,
//                     decoration: const BoxDecoration(
//                       color: Color(0xffe0e0e0),
//                       shape: BoxShape.circle,
//                     ),
//
//                     child: IconButton(
//                       onPressed: () {},
//                       icon: Icon(
//                         iconData,
//                         color: Colors.black87,
//                         size: 35.0,
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Text(
//                       widget.text,
//                       style: const TextStyle(
//                         fontSize: 16,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         );
//   }
// }
