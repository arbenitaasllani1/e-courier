import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  final String imageUrl;
  final String title;
  final double rating;
  final double price;
  final String itemSold;

  const ProductWidget({
    Key? key,
    required this.imageUrl,
    required this.title,
    required this.rating,
    required this.price,
    required this.itemSold,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: AspectRatio(
                aspectRatio: 3 / 4,
                child: Image.asset(
                  imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(height: 8),
            Text(
              title,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            Row(
              children: [
                const Icon(
                  Icons.star_half_outlined,
                  color: Colors.black,
                  size: 18,
                ),
                const SizedBox(width: 4),
                Text(
                  rating.toString(),
                  style: const TextStyle(
                    fontSize: 14,
                    color: Colors.grey
                  ),
                ),
                const SizedBox(width: 10),
                Container(
                  width: 70.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: const Color(0xffe0e0e0)
                  ),
                  child: Center(
                    child: Text(
                      itemSold.toString(),
                      style: const TextStyle(
                          fontSize: 11
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8),
            Text(
              '\$${price.toStringAsFixed(2)}',
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
