import 'package:ecourier/features/screens/cart/cart_screen.dart';
import 'package:ecourier/features/screens/home/home_screen.dart';
import 'package:ecourier/features/screens/orders/orders_screen.dart';
import 'package:ecourier/features/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {
  final int selectedIndex;
  final void Function(int index) onItemTapped;

  const BottomNavigation({
    Key? key,
    required this.selectedIndex,
    required this.onItemTapped,
  }) : super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {

    final List<Widget> pages = [
      const HomeScreen(),
      const CartScreen(cartProducts: []),
      const OrdersScreen(),
      // const WalletScreen(),
      const ProfileScreen(),
    ];

    return BottomNavigationBar(
      currentIndex: widget.selectedIndex,
      onTap: widget.onItemTapped,
      items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                      Icons.home,
                    color: Colors.black45,
                    size: 28.0,
                  ),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.shopping_bag_outlined,
                    color: Colors.black45,
                    size: 28.0,
                  ),
                  label: 'Cart',

                ),

                BottomNavigationBarItem(
                  icon: Icon(
                      Icons.shopping_cart_outlined,
                    color: Colors.black45,
                    size: 28.0,

                  ),
                  label: 'Orders',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                      Icons.wallet,
                    color: Colors.black45,
                    size: 28.0,

                  ),
                  label: 'Wallet',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                      Icons.person_outline,
                    color: Colors.black45,
                    size: 28.0,
                  ),
                  label: 'Profile',
                ),
      ],
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.black45,
      unselectedLabelStyle: const TextStyle(color: Colors.black45),
      selectedLabelStyle: const TextStyle(color: Colors.black),
    );
  }
}