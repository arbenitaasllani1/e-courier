import 'package:ecourier/features/models/product.dart';
import 'package:flutter/material.dart';

class ProductDetailsScreen extends StatelessWidget {
  final Product product;

  const ProductDetailsScreen({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: const EdgeInsets.all(16.0),
        children: [
          AspectRatio(
            aspectRatio: 3 / 4,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.asset(
                product.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          ListTile(
            title: Text(
              product.title,
              style: const TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          ListTile(
            title: const Text(
              'Price:',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              '\$${product.price.toStringAsFixed(2)}',
              style: const TextStyle(fontSize: 16.0),
            ),
          ),
          const SizedBox(height: 16.0),
          Center(
            child: IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () {
                _deleteProductFromWishlist(context, product);
              },
            ),
          ),
        ],
      ),
    );
  }

  void _deleteProductFromWishlist(BuildContext context, Product product) {
    print('Deleting product ${product.title} from wishlist...');
    Navigator.pop(context);
  }
}
