import 'dart:convert';
// import 'dart:ui';
import 'package:ecourier/features/models/product.dart';
import 'package:flutter/material.dart';
import 'package:ecourier/features/widgets/selector/size_selector.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShoesDetailScreen extends StatefulWidget {
  final String imageUrl;
  final String title;
  final double rating;
  final double price;
  final String itemSold;
  final String description;
  final String selectedSize;

  const ShoesDetailScreen({
    Key? key,
    required this.imageUrl,
    required this.title,
    required this.rating,
    required this.price,
    required this.itemSold,
    required this.description,
    required this.selectedSize,
  }) : super(key: key);

  @override
  _ShoesDetailScreenState createState() => _ShoesDetailScreenState();
}

class _ShoesDetailScreenState extends State<ShoesDetailScreen> {
  int quantity = 1;
  bool isFavorite = false;
  List<Product> favoriteProducts = [];

  @override
  void initState() {
    super.initState();
    loadFavoriteProducts();
  }

  Future<void> loadFavoriteProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? favoriteProductStrings =
        prefs.getStringList('favoriteProducts');
    if (favoriteProductStrings != null) {
      setState(() {
        favoriteProducts = favoriteProductStrings
            .map((jsonString) => Product.fromJson(json.decode(jsonString)))
            .toList();
        isFavorite =
            favoriteProducts.any((product) => product.title == widget.title);
      });
    }
  }

  Future<void> saveFavoriteProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favoriteProductStrings =
        favoriteProducts.map((product) => product.toJsonString()).toList();
    await prefs.setStringList('favoriteProducts', favoriteProductStrings);
  }

  void togglefavorite() {
    setState(() {
      isFavorite = !isFavorite;
      if (isFavorite) {
        favoriteProducts.add(Product(
          imageUrl: widget.imageUrl,
          title: widget.title,
          price: widget.price,
        ));
      } else {
        favoriteProducts
            .removeWhere((product) => product.title == widget.title);
      }
      saveFavoriteProducts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                AspectRatio(
                  aspectRatio: 3 / 4,
                  child: Image.asset(
                    widget.imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: 10,
                  left: 10,
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.title,
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(
                    isFavorite ? Icons.favorite : Icons.favorite_border,
                    color: isFavorite ? Colors.black : Colors.grey,
                    size: 30.0,
                  ),
                  onPressed: () {
                    togglefavorite();
                  },
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Container(
                  width: 70.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: const Color(0xffe0e0e0),
                  ),
                  child: Center(
                    child: Text(
                      widget.itemSold.toString(),
                      style: const TextStyle(fontSize: 11),
                    ),
                  ),
                ),
                const SizedBox(width: 10.0),
                const Icon(Icons.star_half, color: Colors.black),
                const SizedBox(width: 5),
                Text(
                  '${widget.rating}',
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(width: 10),
              ],
            ),
            const Divider(
              color: Colors.black26,
            ),
            Column(
              children: [
                const Text(
                  "Description",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                Text(
                  widget.description.toString(),
                  style: const TextStyle(fontSize: 14),
                ),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                SizeSelector(
                  sizes: const ['36', '37', '38', '39', '40'],
                  selectedSize: widget.selectedSize,
                  onSizeSelected: (selectedSize) {},
                ),
                const SizedBox(width: 20),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              children: [
                const Text(
                  "Quantity",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                const SizedBox(width: 20),
                IconButton(
                  onPressed: () {
                    setState(() {
                      quantity = quantity == 1 ? 1 : quantity - 1;
                    });
                  },
                  icon: const Icon(Icons.remove),
                  color: Colors.black,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: const Color(0xffe0e0e0),
                  ),
                  child: Text(
                    '$quantity',
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      quantity++;
                    });
                  },
                  icon: const Icon(Icons.add),
                  color: Colors.black,
                ),
              ],
            ),
            const SizedBox(height: 5),
            const Divider(
              color: Colors.black26,
            ),
            Row(
              children: [
                Column(
                  children: [
                    const Text("Total price"),
                    Text(
                      ' \$${(widget.price * quantity).toStringAsFixed(2)}',
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                const SizedBox(width: 20.0),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 10.0),
                    height: 50.0,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.shopping_bag, color: Colors.white),
                          SizedBox(width: 10),
                          Text(
                            'Add to Cart',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
