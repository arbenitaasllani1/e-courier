import 'dart:convert';

class Product {
  final String imageUrl;
  final String title;
  final double price;

  Product({
    required this.imageUrl,
    required this.title,
    required this.price,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      imageUrl: json['imageUrl'],
      title: json['title'],
      price: json['price'].toDouble(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'imageUrl': imageUrl,
      'title': title,
      'price': price,
    };
  }

  String toJsonString() {
    return json.encode(toJson());
  }
}
