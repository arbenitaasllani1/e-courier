import 'dart:async';
import 'package:ecourier/features/screens/products/clothes_screen.dart';
import 'package:ecourier/features/screens/home/home_screen.dart';
import 'package:ecourier/features/screens/intro/intro_page.dart';
import 'package:ecourier/features/screens/login_register/register_screen.dart';
import 'package:ecourier/features/screens/login_register/start_screen.dart';
import 'package:ecourier/features/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: SplashScreen(),
      home: HomeScreen(),
      // home: ClothesScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {

    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //   showDialog(context: context, builder: (context){
    //     return AlertDialog(content: Text("sdfsd"),);
    //   });
    // });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

      Timer(
        const Duration(seconds: 2),
        () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => IntroPage(),
            ),

          );
        },
      );


    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: Image.asset('images/logo.png'),
          ),
          // Align(
          //   alignment: Alignment.bottomCenter,
          //   child: Padding(
          //     padding: const EdgeInsets.only(bottom: 16.0),
          //      child: LoadingAnimationWidget(),
          //   ),
          // ),
        ],
      ),
    );
  }
}

